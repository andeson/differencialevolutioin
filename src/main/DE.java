package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.DoubleStream;

public class DE {

	public enum Function {
		F1, F6, F7, F8, F17, F19;
	}

	List<Individual> population;
	int POPULATION_SIZE = 50;
	double CROSSOVER_PROBABILITY = 0.4;
	double DIFFERENTIAL_WEIGHT = 1.2d;
	int ITERATION_NO = 10_000;

	Random random;
	PrintWriter pw;

	public static void main(String[] args) {

		double[] dimension1Bounds = new double[2];
		dimension1Bounds[0] = -100;
		dimension1Bounds[1] = 100;

		List<double[]> dimensionList = new LinkedList<>();

		dimensionList.add(dimension1Bounds);

		DE de = new DE();

		// inicia o processo de otimização e retorna o melhor candidato depois da
		// quantidade de interações especificada
		try {
			PrintWriter p = new PrintWriter(new File("data/DE_exe.csv"));
			p.write("F1;F6;F7;F8");
			p.write("\r\n");
			for (int i = 0; i < 10; i++) {

				Individual f1Result = new DE().exe(dimensionList, Function.F1);
				Individual f6Result = new DE().exe(dimensionList, Function.F6);
				Individual f7Result = new DE().exe(dimensionList, Function.F7);
				Individual f8Result = new DE().exe(dimensionList, Function.F8);
				p.write(String.valueOf(f1Result) + ";" + String.valueOf(f6Result) + ";" + String.valueOf(f7Result) + ";"
						+ String.valueOf(f8Result));
				p.write("\r\n");
				System.out.println("Fitness for F1 interaction " + i + ": " + f1Result);
				System.out.println("Fitness for F6 interaction " + i + ": " + f6Result);
				System.out.println("Fitness for F7 interaction " + i + ": " + f7Result);
				System.out.println("Fitness for F8 interaction " + i + ": " + f8Result);

			}
			p.flush();
		} catch (FileNotFoundException ex) {

		}
		/*
		 * new Thread(() -> { System.out.println("F1 running..."); for (int i = 0; i <
		 * 10; i++) { System.out.println("Best combination found for F1 interaction " +
		 * i + ": " + new DE().exe(dimensionList, Function.F1)); }
		 * 
		 * }).start();
		 * 
		 * new Thread(() -> { System.out.println("F6 running..."); for (int i = 0; i <
		 * 10; i++) { System.out.println("Best combination found for F6 interaction " +
		 * i + ": " + new DE().exe(dimensionList, Function.F6)); }
		 * 
		 * }).start();
		 * 
		 * new Thread(() -> { System.out.println("F7 running..."); for (int i = 0; i <
		 * 10; i++) { System.out.println("Best combination found for F7 interaction " +
		 * i + ": " + new DE().exe(dimensionList, Function.F7)); }
		 * 
		 * }).start();
		 * 
		 * new Thread(() -> { System.out.println("F8 running..."); for (int i = 0; i <
		 * 10; i++) { System.out.println("Best combination found for F8 interaction " +
		 * i + " : " + new DE().exe(dimensionList, Function.F8)); }
		 * 
		 * }).start();
		 */

	}

	public double fitFunction(Individual aCandidate, Function f) {

		double value = 10.0d * aCandidate.dataValue.length;

		switch (f) {
		case F1:
			return F1(aCandidate.dataValue);
		case F6:
			return F6(aCandidate.dataValue);
		case F7:
			return F7(aCandidate.dataValue);
		case F8:
			return F8(aCandidate.dataValue);
		case F17:
			return F17(aCandidate.dataValue);
		case F19:
			return F19(aCandidate.dataValue);

		default:
			break;
		}

		return value;
	}

	private double F1(double[] input) {
		double funcao = 0.0;
		for (int i = 0; i < input.length; i++) {
			funcao += input[i] * input[i];
		}
		return (funcao);
	}

	private double F6(double[] input) {
		double F = 0.0;
		for (int i = 0; i < input.length - 1; i++) {
			F += 100 * Math.pow((Math.pow(input[i], 2) - input[i + 1]), 2) + Math.pow((input[i] - 1), 2);
		}
		return F;
	}

	private double F7(double[] input) {
		double sum = 0;
		for (int i = 0; i < input.length - 1; i++) {
			double x = input[i];
			double x1 = input[i + 1];
			double si = Math.sqrt(x * x + x1 * x1);

			double sinTerm = Math.sin(50 * Math.pow(si, 0.2));
			sum += Math.sqrt(si) + Math.sqrt(si) * sinTerm * sinTerm;
		}
		sum /= input.length - 1;
		return sum * sum;
	}

	private double F8(double[] input) {
		double sum1 = 0.0, sum2 = 0.0, result;
		for (int i = 0; i < input.length; ++i) {
			sum1 += input[i] * input[i];
			sum2 += Math.cos(2.0 * Math.PI * input[i]);
		}
		sum1 = -0.2 * Math.sqrt(sum1 / input.length);
		sum2 /= input.length;
		result = 20.0 + Math.E - 20.0 * Math.exp(sum1) - Math.exp(sum2);
		return result;
	}

	private double F17(double[] vector) {
		return 0;
	}

	private double F19(double[] vector) {
		return 0;
	}

	public DE() {
		random = new Random();
		population = new LinkedList<>();
	}

	public Individual exe(List<double[]> dimensionList, Function f) {

		for (int i = 0; i < POPULATION_SIZE; i++) {
			Individual individual = new Individual(dimensionList);
			population.add(individual);
		}

		for (int iterationCount = 0; iterationCount < ITERATION_NO; iterationCount++) {

			try {
				pw = new PrintWriter(new File("data/" + f + ".csv"));
			} catch (FileNotFoundException ex) {

			}

			for (int n = 0; n < dimensionList.size(); n++) {
				pw.write("v" + Integer.toString(n));
				pw.write(";");
			}

			pw.write("fValue");
			pw.write("\r\n");

			for (Individual individual : population) {
				pw.write(individual.toString());
				pw.write(";");
				pw.write(Double.toString(fitFunction(individual, f)));
				pw.write("\r\n");
			}

			pw.flush();
			int loop = 0;

			while (loop < population.size()) {

				Individual original = null;
				Individual candidate = null;
				boolean boundsHappy;

				do {

					boundsHappy = true;
					int x = loop;
					int a, b, c = -1;

					do {
						a = random.nextInt(population.size());
					} while (x == a);
					do {
						b = random.nextInt(population.size());
					} while (b == x || b == a);
					do {
						c = random.nextInt(population.size());
					} while (c == x || c == a || c == b);

					Individual individual1 = population.get(a);
					Individual individual2 = population.get(b);
					Individual individual3 = population.get(c);

					Individual noisyRandomCandicate = new Individual(dimensionList);

					for (int n = 0; n < dimensionList.size(); n++) {
						noisyRandomCandicate.dataValue[n] = (individual1.dataValue[n]
								+ DIFFERENTIAL_WEIGHT * (individual2.dataValue[n] - individual3.dataValue[n]));
					}

					original = population.get(x);
					candidate = new Individual(dimensionList);

					for (int n = 0; n < dimensionList.size(); n++) {
						candidate.dataValue[n] = original.dataValue[n];
					}

					int R = random.nextInt(dimensionList.size());

					for (int n = 0; n < dimensionList.size(); n++) {

						double crossoverProbability = random.nextDouble();

						if (crossoverProbability < CROSSOVER_PROBABILITY || n == R) {
							candidate.dataValue[n] = noisyRandomCandicate.dataValue[n];
						}

					}

					for (int n = 0; n < dimensionList.size(); n++) {
						if (candidate.dataValue[n] < dimensionList.get(n)[0]
								|| candidate.dataValue[n] > dimensionList.get(n)[1]) {
							boundsHappy = false;
						}
					}

				} while (boundsHappy == false);

				if (fitFunction(original, f) < fitFunction(candidate, f)) {
					population.remove(original);
					population.add(candidate);
				}
				loop++;
			}
		}

		Individual bestFitness = new Individual(dimensionList);

		for (int i = 0; i < population.size(); i++) {
			Individual individual = population.get(i);

			if (fitFunction(bestFitness, f) < fitFunction(individual, f)) {

				try {
					bestFitness = (Individual) individual.clone();
				} catch (CloneNotSupportedException ex) {

				}
			}
		}

		return bestFitness;
	}

	public class Individual implements Cloneable {

		double[] dataValue;

		public Individual(List<double[]> dimensionIn) {
			int noDimension = dimensionIn.size();
			dataValue = new double[noDimension];

			for (int dimensionIndex = 0; dimensionIndex < noDimension; dimensionIndex++) {
				double dimensionLowerBound = dimensionIn.get(dimensionIndex)[0];
				double dimensionUpperBound = dimensionIn.get(dimensionIndex)[1];
				DoubleStream valueGenerator = random.doubles(dimensionLowerBound, dimensionUpperBound);
				dataValue[dimensionIndex] = valueGenerator.iterator().nextDouble();
			}
		}

		@Override
		public String toString() {

			String string = "";

			for (int i = 0; i < dataValue.length; i++) {
				string += Double.toString(dataValue[i]);

				if ((i + 1) != dataValue.length) {
					string += ";";
				}
			}

			return string;
		}

		@Override
		protected Object clone() throws CloneNotSupportedException {
			return super.clone();
		}
	}

}
